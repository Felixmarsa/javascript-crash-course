alert('Hello World');

console.log('Hello World');
console.error('This is an error');
console.warn('This is a warning');

// var, let, const
// let, const
let age = 30;
age = 31;

console.log(age);

//
const score = 10;

console.log(score);

// String, Numbers, Boolean, null, undefined, Symbol

const name= 'John';
const agee = 30;
const rating = 4.5;
const isCool = true;
const x = null;
const y = undefined;
let z;

console.log(typeof name);
console.log(typeof age);
console.log(typeof rating);
console.log(typeof isCool);
console.log(typeof x);
console.log(typeof y);
console.log(typeof z);

//Contenation
console.log('My name is' +name+ ' and i am' + age);
// Template String
console.log('My name is ${name} and i am ${age}')

console.log(hello);

//

const s = 'Hello World';

console.log(s.length);
console.log(s.toUpperCase());
console.log(s.toLowerCase());
console.log(s.substring(0, 5));


const c = 'technology, computers, it, code';
console.log(c.split(''));

//Arrays -  variable that hold multiple values

const numbers = new Array(1,2,3,4,5);

const fruits = ['apples','oranges','pears', 10, true];

console.log(fruits);
console.log(fruits[1]);
fruits[3]='grapes';
fruits.push('mangos');
fruits.unshift('strawberries');

fruits.pop();

console.log(Array.isArray('hello'));

console.log(fruits.indexOf('oranges'));

console.log(numbers);

/* multi'line
comment*/

const person ={
    firstName: 'John',
    lastName: 'Doe',
    ageee: 30,
    hobbies: ['music','movie','sports'],
    address:{
        street:'50 main st',
        city:'Boston',
        state: 'MA'
    }
}

console.log(person.firstName, person, lastName);
console.log(person.hobbies[1]);
console.log(person.address.city);

const {firstName, lastName, address: { city }} = person;

console.log(city);
person.email ='john@gmail.com';

console.log(person);

//

const todos = [
    {
        id: 1,
        text: 'Take out trash',
        isCompleted: true
    },
    {
        id: 2,
        text: 'Meeting with boss',
        isCompleted: true
    },
    {
        id: 3,
        text: 'Dentist appt',
        isCompleted: false
    }
];

for(let i = 0; i < todos.length; i++){
    console.log(todos[i].text)
}

for(let todo of todos){
    console.log(todo.text);
}

console.log(todos[1].text);


const todoJSON = JSON.stringly(todos);
console.log(todoJSON);

// For 
for(let i = 0; i <= 10; i++){
    console.log('for Loop Number: ${i}');
}

//While
let i =0
while(i < 10){
    console.log();
    i++;
}

//forEach, map, filter
todos.forEach(function(){
    console.log(todo.text);
});

const todoText = todos.map(function(todo){
    return todo.text;
});

console.log(todoText);

const todoCompleted = todos.filter(function(todo){
    return todo.isCompleted === true;
}).map(function(todo){
    return todo.todoText;
})

//

const l = 10;

if(l == 10){
    console.log('x is 10');
}else if(x>10){
    console.log('x is greater than 10');
}else {
    console.log('x is less than 10');
}

if(x > 5 || y > 10){
    console.log('x is more than 5 or y is more than 10');
}

if(x > 5 ){
    if(y > 10){

    }
}

//

const xx = 11;
const color =  xx > 10 ? 'red' : 'blue';

switch(color){
    case'red':
        console.log('color is red');
        break;
    case 'blue':
        console.log('color is blue');
        break;
    default:
        console.log('color is NOT red or blue');
        break;
}

//

function addNums(num1, num2){
    console.log(num1 + num2);
}

addNums(5, 5);

function addNums(num1, num2){
    return num1 + num2;
}

console.log(addNums(5, 5));

const addNums = (num1 = 1, num2 = 2) => {
    console.log(num1 + num2);
}

addNums(5, 5);

const addNums = (num1 = 1, num2 = 2) => num1 + num2;

console.log(addNums(5));

todos.forEach((todo) => console.log(todo));

// Constructor funtion

function Person(firstName, lastName, dob){
    this.firstName = firstName;
    this.lastName = lastName;
    this.dob = dob;
    this.getBirthYear = function(){
        return this.dob.getFullYear();
    }
    this.getFullName = function(){
            return '${this.firstName} ${this.lastName}';
    }
}

//class
class Person {
    constructor(firstName, lastName, dob) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = new Date(dob);
    }

    getBirthYear(){
        return this.dob.getFullYear();
    }

    getFullName(){
        return '${this.firstName} ${this.lastName}';
    }
}

// Instantiate object
const person1 = new Person('John', 'Doe','4-3-1980');
const person2 = new Person('Mary', 'Smith','3-6-1970');

console.log(person2.dob.getFullYear());
console.log(person1);
console.log(person1);

// ELEMENT SELECTORS

// Single Element Selectors
console.log(document.getElementById('my-form'));
console.log(document.querySelector('.container'));
// Multiple Element Selectors
console.log(document.querySelectorAll('.item'));
console.log(document.getElementsByTagName('li'));
console.log(document.getElementsByClassName('item'));

const items = document.querySelectorAll('.item');
items.forEach((item) => console.log(item));


// MANIPULATING THE DOM
const ul = document.querySelector('.items');
// ul.remove();
// ul.lastElementChild.remove();
ul.firstElementChild.textContent = 'Hello';
ul.children[1].innerText = 'Brad';
ul.lastElementChild.innerHTML = '<h1>Hello</h1>';

const btn = document.querySelector('.btn');
// btn.style.background = 'red';


// EVENTS

// Mouse Event
btn.addEventListener('click', e => {
  e.preventDefault();
  console.log(e.target.className);
  document.getElementById('my-form').style.background = '#ccc';
  document.querySelector('body').classList.add('bg-dark');
  ul.lastElementChild.innerHTML = '<h1>Changed</h1>';
});

// Keyboard Event
const Inputname = document.querySelector('#name');
nameInput.addEventListener('input', e => {
  document.querySelector('.container').append(nameInput.value);
});


// USER FORM SCRIPT

// Put DOM elements into variables
const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name');
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');

// Listen for form submit
myForm.addEventListener('submit', onSubmit);

function onSubmit(e) {
  e.preventDefault();
  
  if(nameInput.value === '' || emailInput.value === '') {
    // alert('Please enter all fields');
    msg.classList.add('error');
    msg.innerHTML = 'Please enter all fields';

    // Remove error after 3 seconds
    setTimeout(() => msg.remove(), 3000);
  } else {
    // Create new list item with user
    const li = document.createElement('li');

    // Add text node with input values
    li.appendChild(document.createTextNode(`${nameInput.value}: ${emailInput.value}`));

    // Add HTML
    // li.innerHTML = `<strong>${nameInput.value}</strong>e: ${emailInput.value}`;

    // Append to ul
    userList.appendChild(li);

    // Clear fields
    nameInput.value = '';
    emailInput.value = '';
  }
}